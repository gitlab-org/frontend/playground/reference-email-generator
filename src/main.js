import Vue from 'vue';
import App from './javascripts/App.vue';

new Vue({
  el: '#app',
  render: h => h(App)
});